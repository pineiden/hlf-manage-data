######
from itertools import groupby
from datetime import datetime
from pathlib import Path   # nopep8
import sys  # nopep8

path = Path.cwd()    # nopep8

if str(path) not in sys.path:  # nopep8
    sys.path.append(str(path))  # nopep8
###############
from typing import Tuple
from build_grafo import Vertice, Arista
from grafo_ml.grafo import Grafo, ConfigGrafo
from leer_ppios_activos import (
    PrincipioActivo,
    DatasetPrincipiosActivos,
    OneHot,
    FrozenPrincipioActivo)
import time
from rich import print
import pickle
import platform

#################


print(platform.python_version())


try:
    from crear_prescripciones import Prescripcion, Asignacion
    from crear_prescripciones import Diagnostico
except Exception as e:
    print(e)


ppath = path / "datasets/prescripciones.data"
print(ppath)
databytes = ppath.read_bytes()
prescripciones = []
try:
    prescripciones = pickle.loads(databytes)
    print("PRESCRIPCIONES")
    print("Cantidad prescripciones", len(prescripciones))
except Exception as e:
    print("Error al", e)

p = prescripciones[0]
print(p)


catalogo = path / "datasets/catalogo.data"
databytes = catalogo.read_bytes()
ppios = pickle.loads(databytes)
field = "paracetamol"  # "acetazolamida"
paracetamol = ppios.search(field).lista[0].codigos
print(paracetamol)

dataset_paracetamol = [
    p for p in prescripciones if p.codigo_medicamento in paracetamol]
print("Total ", len(dataset_paracetamol))

databytes = pickle.dumps(dataset_paracetamol)
paracetamol_path = path / f"{field}.data"
paracetamol_path.write_bytes(databytes)
print("Ok, saved")


def create_grafo(name, group, config) -> Tuple[str, Grafo, Path]:
    g = Grafo.create(name, group, config)
    return name, g, path


p = prescripciones[-1]
campos = p.categorias
pesos = p.pesos
limite = 0.45
print(campos, pesos, limite)
try:
    config = ConfigGrafo(
        campos,
        Vertice,
        Arista,
        pesos,
        limite)
except Exception as e:
    print(e)
print(config)
vertices = {Vertice(p.to_hashable()) for p in dataset_paracetamol}


name = f"Grafo {field}"
start = datetime.utcnow()
# g = Grafo.create(name, vertices, config)
end = datetime.utcnow()
delta = (end-start).total_seconds()
print(f"Duracion {len(vertices)}", name, "Tiempo", delta)

counter = 0

dataset_paracetamol.sort(key=lambda e: e.diagnostico.texto)
for diagnostico, grupo in groupby(dataset_paracetamol, key=lambda e: e.diagnostico.texto):
    print(diagnostico, len(list(grupo)))
    counter += 1

print(counter)
