from pydantic.dataclasses import dataclass
import re


@dataclass(frozen=True)
class Dosis:
    cantidad: float
    unidad_medida: str
    descripcion: str

    @classmethod
    def create(cls, texto):
        # mu = 'µ'
        # muutf8 = "".join([f"\u0{m}" for m in mu.encode("utf-8")])
        texto = texto.replace("µ", "u").strip()
        regex = re.compile(
            f"(\D+)?(\d*\.?\d*) ?(u?m?g?l?%?\/?(\d* )?\w*)?(u\.i\.\/?(\d* )?m?l?)?(.*)?", re.UNICODE)
        data = {"descripcion": "", "cantidad": 0, "unidad_medida": ""}
        match = re.match(regex, texto)
        if len(match.group(2)) > 0:
            groups = list(match.groups())
            if len(groups) > 0:
                descripcion = groups[-1]
                if groups[0]:
                    descripcion = f"{groups[0]} - {groups[-1]}"
                unidad_medida = groups[2]
                cantidad = float(groups[1])
                data["descripcion"] = str(descripcion).strip()
                data["cantidad"] = cantidad
                data["unidad_medida"] = str(unidad_medida)

        return cls(**data)

    def contains(self, texto):
        if texto in self.descripcion:
            return True
        return False
