import csv
import asyncio
from datetime import datetime
import multiprocessing as mp
import pickle
from pathlib import Path
from prescripcion import Prescripcion
from asignacion import Asignacion
from rich import print
import time
from grafo_ml.vertice import AbstractVertice
from grafo_ml.arista import AbstractArista
from grafo_ml.grafo import Grafo, ConfigGrafo
from attrs import define, fields
from typing import List, Tuple, Dict, Any
import collections
from leer_ppios_activos import DatasetPrincipiosActivos
from principio_activo import PrincipioActivo, FrozenPrincipioActivo
from one_hot import OneHot
from one_hot_vector import OneHotVector
from itertools import groupby
from multiprocessing import Process, Pool
import concurrent.futures
import typer
import json

Pair = Tuple[str, Any]
PrescripcionDict = Tuple[Pair, ...]


def read_prescripciones():
    ppath = Path.cwd() / "datasets/prescripciones.data"
    print(ppath.exists())
    databytes = ppath.read_bytes()
    prescripciones = pickle.loads(databytes)
    return prescripciones


class Vertice(AbstractVertice):

    @property
    def fields(self):
        return set(self.element)

    @property
    def numeric_fields(self):
        """
        Entrega los elementos numericos para el objeto elemento pasado
        a diccionario
        """
        Numeric = {int, float}
        return {k for k, v in self.dict_element.items() if type(v) in Numeric}

    @property
    def element_class(self):
        return PrescripcionDict

    def __len__(self):
        return len(self.dict_element.values())

    @property
    @classmethod
    def categorias(cls):
        return Prescripcion.categorias

    @property
    @classmethod
    def pesos(cls):
        return Prescripcion.pesos

    def get(self, field: str):
        return self.dict_element.get(field)

    @property
    def dict_element(self):
        return dict(self.element)

    @property
    def values(self):
        return tuple([self.get(k) for k in self.numeric_fields])

    @property
    def one_hot_fields(self):
        return tuple([self.get(k) for k in self.one_hot])


def otsuka_ochiai(oh1, oh2):
    return len(oh1 * oh2) / (len(oh1) * len(oh2))


def similitud(
        a: Vertice,
        b: Vertice) -> float:
    # preparar los campo onehots

    # a * b / (len(a) * len(b))

    pass


class Arista(AbstractArista):
    def similitud(self) -> float:
        return similitud(self.a, self.b)


def save_grafo(name, grafo):
    databytes = pickle.dumps(grafo)
    name = name.replace(" ", "_")
    path = Path.cwd() / "grafos" / f"grafo_{name}.model"
    path.write_bytes(databytes)
    print("Saved grafo", path)
    return path


def read_grafo(name, path):
    databytes = path.read_bytes()
    grafo = pickle.loads(databytes)
    return grafo


def create_grafo(name, group, config) -> Tuple[str, Grafo]:
    print(f"Graph for {name}")
    path = Path.cwd() / "grafos" / f"grafo_{name}.model"
    if not path.exists():
        try:
            g = Grafo.create(name, group, config)
        except Exception as e:
            print(e)
            raise e
        path = save_grafo(name, g)
        return name, g, path
    #g = read_grafO
    return name, None, path


def crear_grupo_grafos(grupo, config, queue: mp.Queue):
    paths = []
    for k, vertices in grupo.items():
        st = time.time()
        start = datetime.utcnow()
        print("Iniciando", k, "Vertices:", len(vertices))
        k, g, p = create_grafo(k, vertices, config)
        end = datetime.utcnow()
        total = (end-start).total_seconds()
        if g:
            print("Terminado", k, f"Vertices: {len(vertices)}", total)

            enviar = {"name": k,
                      "vertices": len(vertices),
                      "timing": total,
                      "start": start.isoformat(),
                      "end": end.isoformat()}
            print("Enviando a queue", queue, "data->", enviar)
            queue.put(enviar)
            print("ya enviado", k)
            paths.append(p)
        del g
        del vertices


def save_stats(queue, writer):
    print("Size queue", queue.qsize())
    if not queue.empty():
        for i in range(queue.qsize()):
            data = queue.get()
            if data:
                print("To csv->", data)
                writer.writerow(data)
                return True
            else:
                print("Cerrando csv")
                return False
    time.sleep(1)
    return True


def run_task(queue):
    now = datetime.now().isoformat()
    path = Path.cwd() / f"stats_grafos_{now}.csv"
    print("Opening stats csv...", path)
    with path.open("w") as ofile:
        writer = csv.DictWriter(
            ofile,
            delimiter=";", fieldnames=("name", "vertices", "timing",
                                       "start", "end"))
        writer.writeheader()
        flag = True
        while flag:
            flag = save_stats(queue, writer)
            ofile.flush()


def hacer_grafo(
        prescripciones: List[Prescripcion],
        nproc: int,
        queue: mp.Queue) -> Dict[str, Grafo]:
    p = prescripciones[-1]
    campos = p.categorias
    pesos = p.pesos
    limite = 0.45
    base = Path.cwd() / "grafos"
    base.mkdir(parents=True, exist_ok=True)
    # crear grafo

    config = ConfigGrafo(
        campos,
        Vertice,
        Arista,
        pesos,
        limite)
    vertices = {Vertice(p.to_hashable()) for p in prescripciones}
    vertices = sorted(vertices, key=lambda v: v.get("principio"))

    procs = []

    args = []
    results = []
    print("Creating graphs...")
    grupos = {i: {} for i in range(nproc)}
    counter = 0

    cantidades = []
    agrupacion = {}
    for key, group in groupby(vertices,
                              key=lambda v: v.get("principio")):
        group = set(group)
        cantidades.append((key, len(group)))
        agrupacion[key] = group

    cantidades.sort(key=lambda x: x[1])
    print(cantidades)

    for key, ngroup in cantidades:
        group_vertices = agrupacion[key]
        grupos[counter][key] = group_vertices
        counter += 1
        if counter == nproc-1:
            counter = 0

    print("Grupos ok, corriendo procesos")
    for k, v in grupos.items():
        print(k, len(v))

    with concurrent.futures.ThreadPoolExecutor() as executor:

        fut = executor.submit(run_task, queue)
        results.append(fut)
        for i in range(nproc-1):
            print("Running process", i)
            fut = executor.submit(crear_grupo_grafos, grupos[i],
                                  config, queue)
            results.append(fut)

        print("Calling futures....")
        total = [r.result() for r in results]
        if total:
            queue.put({})
    print("Resultados", len(results))
    return dict(results[1::])


def calcular_similitud(grafo):
    pass


def entrenar(grafo):
    pass


def run():  # nproc: int):
    queue = mp.Queue()
    prescripciones = read_prescripciones()
    for p in prescripciones:
        print(p)
    #grafos = hacer_grafo(prescripciones, nproc, queue)
    # for i, grafo in enumerate(grafos.keys()):
    #    print(i, grafo.schema.keys())


if __name__ == '__main__':
    typer.run(run)
