import pickle
from pathlib import Path
from leer_ppios_activos import (OneHot)
from prescripcion import Prescripcion
from asignacion import Asignacion
from diagnostico import Diagnostico
from principio_activo import PrincipioActivo, FrozenPrincipioActivo
from rich import print
import time
import json


def convert(item):
    if isinstance(item, OneHot):
        return item.value
    else:
        return str(item)


if __name__ == '__main__':

    ppath = Path.cwd() / "datasets/prescripciones.data"

    databytes = ppath.read_bytes()
    prescripciones = pickle.loads(databytes)
    dataset = list(map(lambda e: e.to_dict, prescripciones))
    json_ppath = ppath.parent / "prescripciones.json"
    datatext = json.dumps(dataset, default=convert)
    json_ppath.write_text(datatext)
