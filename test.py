import re
from rich import print
from dataclasses import dataclass
from leer_ppios_activos import Dosis

name = 'fenoterol bromhidrato + ipatropio bromuro'
texto = """fenoterol bromhidrato 0.5 mg/ml + ipatropio bromuro 0.25 mg/ml solución para nebulizar frasco 20 ml, fenoterol bromhidrato 50 µg/dosis+ipatropio bromuro 20 µg/dosis solución para inhalación en envase a presión"""

if '+' in name:
    names = name.split("+")
    medicamentos = {k.strip(): [] for k in names}

    valores = []
    [valores.extend([m.strip() for m in v.strip().split("+")])
     for v in texto.split(",")]
    for k, values in medicamentos.items():
        for v in valores:
            if k in v:
                info = v.replace(k, "").strip()
                dosis = Dosis.create(info)
                print(dosis)
                values.append(info)
    print(medicamentos)
