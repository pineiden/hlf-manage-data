from pathlib import Path
import pickle
from leer_ppios_activos import (
    PrincipioActivo,
    DatasetPrincipiosActivos)
from one_hot import OneHot
from riesgo import Riesgo
from dosis import Dosis
from one_hot import OneHot
from prioridad import Prioridad
from categoria import Categoria

from rich import print

catalogo = Path.cwd() / "datasets/catalogo.data"
databytes = catalogo.read_bytes()
ppios = pickle.loads(databytes)
print(ppios)
ppio = ppios.mapa.get("FANN02016")
print(ppio)
