from attrs import field, define
from pydantic.dataclasses import dataclass
from pathlib import Path
import pickle
import numpy as np
from one_hot_vector import OneHotVector
from analisis_nlp import op_words
from functools import reduce


def read_diagnosticos():
    ppath = Path.cwd() / "datasets"
    path = ppath / "diags_map.data"
    databytes = path.read_bytes()
    return pickle.loads(databytes)


CATALOGO = read_diagnosticos()


@define(slots=True, frozen=True)
class Diagnostico:
    texto: str
    one_hot: OneHotVector

    @classmethod
    def create(cls, texto):
        try:
            # vector of indexes
            # se recupera el vector de indexes en que el texto está
            bag = CATALOGO["bag"]
            n = len(bag)
            vector = CATALOGO["vectores"].get(texto, np.zeros(n))
            vector = np.array([1 if v else 0 for v in vector])
            one_hot = OneHotVector(bag, vector)
            return cls(texto, one_hot)
        except Exception as e:
            print("Error al crear Diagnostico")
            raise e


if __name__ == "__main__":
    code = ""
    bag = CATALOGO["bag"]
    # diag = Diagnostico.create()
    for name, vec in CATALOGO["vectores"].items():
        diag = Diagnostico.create(name)
        print(diag)
        # breakpoint()
