from pydantic.dataclasses import dataclass
from principio_activo import (
    PrincipioActivo,
    FrozenPrincipioActivo)
from leer_ppios_activos import DatasetPrincipiosActivos
from one_hot_vector import OneHotVector
from diagnostico import Diagnostico
from datetime import datetime
from asignacion import Asignacion
import numpy as np
from attrs import define, field
from slugify import slugify

START_DATE = datetime(2015, 1, 1)


@define(slots=True, frozen=True)
class Prescripcion:
    index: int
    fecha: datetime
    codigo_receta: str
    codigo_hospitalizacion: str
    codigo_medicamento: str
    asignacion: Asignacion
    principio: FrozenPrincipioActivo
    especialidad: str
    code_especialidad: OneHotVector
    code_receta: OneHotVector
    # diagnostico: Diagnostico

    @property
    def one_hot_fields(self):
        return (
            self.principio.one_hot,  # ppio
            self.principio.hlf.one_hot,  # presentacion
            self.code_especialidad,  # especialidad related
            self.code_receta,  # receta related
            # self.diagnostico.one_hot,
        )

    @property
    def total_time(self):
        diff = self.fecha - START_DATE
        return diff.total_seconds()

    @property
    def numeric_fields(self):
        return np.array((self.asignacion.fields,
                         self.total_time,
                         *self.date_expand,
                         self.principio.prioridad.value,
                         self.principio.riesgo.value))

    @property
    def date_expand(self):
        return {"dt_year": self.fecha.year, "dt_month": self.fecha.month, "dt_day": self.fecha.day}

    @property
    def categorias(self):
        return ["principio"]

    @property
    def pesos(self):
        """
        Estos pesos establecen un enlace por coincidencia en el
        grafo.
        Si la suma de los pesos existentes en todos los campos da un
        valor límite, se establece la arista.

        Todo debe sumar 1 para tener el 100%
        """
        return {
            # recetas ascoiadas
            "codigo_receta": 5/100,
            # onehot ppio activo
            "medicamento": 25/100,
            # one hot presentacion medicamento
            "one_hot_hlf": 20/100,
            # especialidad que receta
            "especialidad": 20/100,
            # suma 0.2
            "frecuencia": 15/100,
            "cantidad": 10/100,
            "durante": 5/100,
            # one hot codigo presentacion de medicina
        }

    def to_dict(self):
        col_ppio = {f"col_ppio_{slugify(k)}": v for k,
                    v in self.principio.one_hot.todict().items()}

        col_esp = {f"col_esp_{slugify(k)}": v for k,
                   v in self.code_especialidad.todict().items()}

        col_rec = {f"col_rec_{slugify(k)}": v for k,
                   v in self.code_receta.todict().items()}
        return {
            "col_riesgo": self.principio.riesgo.value,
            "col_prioridad": self.principio.prioridad.value,
            "col_presentacion": self.get_hlf_oh().toint,
            **self.date_expand,
            **col_ppio,
            **col_esp,
            **col_rec,
            **self.asignacion.todict()
        }

    def get_hlf_oh(self):
        code = self.codigo_medicamento
        return self.principio.one_hot_hlf(self.codigo_medicamento)
