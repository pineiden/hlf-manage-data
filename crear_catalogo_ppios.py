from pathlib import Path
from leer_ppios_activos import (DatasetPrincipiosActivos)
from principio_activo import PrincipioActivo
from one_hot_vector import OneHotVector
import pandas as pd
from rich import print
import pickle
from typing import Callable
import json

ruta = Path.cwd() / "PRINCIPIOS_ACTIVOS_MDS.xlsx"


def onehot_encodig(data, field_callback: Callable):
    chars = len(data)
    tabla = {}
    names = [d.name for d in data]
    for i, elem in enumerate(data):
        tabla[field_callback(elem)] = OneHotVector.create(names, i)
    return tabla


if ruta.exists():
    keys = """ID_FARMACO	COL_CONSULTAR	PRINCIPIO_ACTIVO RIESGO_TEMP
    RIESGO	DESCRIPCION_HLF	CODIGO_HLF PRIORIDAD   CAT_1	CAT_2
    CAT_3	CAT_4	CAT_5	CAT_6""".split()
    book = pd.read_excel(ruta)
    data = pd.DataFrame(book, columns=keys)
    dataset = [PrincipioActivo.create(d)
               for d in data.to_dict(orient='records')]
    dataset.sort(key=lambda x: x.fid)
    onehot_map = onehot_encodig(dataset, field_callback=lambda e: e.fid)
    ppios = DatasetPrincipiosActivos.create(dataset, onehot_map)
    databytes = pickle.dumps(ppios)
    catalogo = Path.cwd() / "datasets/catalogo.data"
    catalogo.write_bytes(databytes)
