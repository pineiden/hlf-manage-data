import re

from attrs import define
from one_hot_vector import OneHotVector
from riesgo import Riesgo
from prioridad import Prioridad
from categoria import Categoria
from dosis import Dosis
from hlf_presentacion import HLF
from typing import List, Dict, Callable, Tuple
from attrs import field, define
from slugify import slugify


@define(slots=True)
class PrincipioActivoBase:
    fid: int
    consultar: int
    name: str
    mode: str
    riesgo: Riesgo
    hlf: HLF
    prioridad: Prioridad
    categorias: Tuple[Categoria, ...]
    one_hot: OneHotVector

    @property
    def todict(self):
        return {
            "fid": self.fid,
            "consultar": self.consultar,
            "name": self.name,
            "mode": self.mode,
            "riesgo": self.riesgo,
            "hlf": self.hlf,
            "prioridad": self.prioridad,
            "categorias": self.categorias,
            "one_hot": self.one_hot
        }

    @property
    def principio(self):
        return slugify(self.name)

    @ property
    def one_hot_str(self):
        return str(self.one_hot)

    def one_hot_hlf(self, code):
        return self.hlf.one_hot(code)

    @ property
    def codigos(self):
        return self.hlf.codigos

    def contains(self, texto):
        texto = texto.lower()
        if texto in self.name:
            return True
        elif any([c.contains(texto) for c in self.categorias]):
            return True
        elif self.hlf.contains(texto):
            return True
        return False

    def check_code(self, code):
        return self.hlf.check_code(code)

    def match_hlf(self, hlf):
        if hlf.descripcion:
            dosis = hlf.descripcion[-1]
            elem = dosis.descripcion.lower()
            for i, h in enumerate(self.hlf.descripcion):
                if elem == h.descripcion and dosis.cantidad == h.cantidad:
                    return i, True
        return 0, False


@define(slots=True)
class PrincipioActivo(PrincipioActivoBase):

    def set_one_hot(self, value: OneHotVector):
        self.one_hot = value

    @classmethod
    def create(cls, data):
        cats = tuple([Categoria(value.capitalize())
                      for k, value in data.items() if k.startswith("CAT")
                      and str(value) != 'nan'])
        pre_name = data.get("PRINCIPIO_ACTIVO", None)
        parens = re.compile("\(.*\)$")
        name = pre_name.lower().strip()
        name = re.sub(parens, "", name).strip()
        mode = pre_name.lower().replace(name, "").replace(
            "(", "").replace(")", "").strip()

        opts = dict(
            fid=data.get("ID_FARMACO", 0),
            consultar=data.get("COL_CONSULTAR", 0),
            name=name,
            mode=mode,
            riesgo=Riesgo.create(data.get("RIESGO_TEMP", 1)),
            hlf=HLF.create(name,
                           data.get("DESCRIPCION_HLF"),
                           data.get("CODIGO_HLF")),
            prioridad=Prioridad.create(data.get("PRIORIDAD", "baja")),
            categorias=cats, one_hot=None)

        return cls(**opts)

    def frozen(self):
        return FrozenPrincipioActivo.create(self)


@define(slots=True, frozen=True)
class FrozenPrincipioActivo:
    fid: int
    consultar: int
    name: str
    mode: str
    riesgo: Riesgo
    hlf: HLF
    prioridad: Prioridad
    categorias: Tuple[Categoria, ...]
    one_hot: OneHotVector

    @ property
    def one_hot_str(self):
        return str(self.one_hot)

    def one_hot_hlf(self, code):
        return self.hlf.one_hot(code)

    @ classmethod
    def create(cls, principio_activo: PrincipioActivo):
        opts = principio_activo.todict
        return cls(**opts)

    @ property
    def codigos(self):
        return self.hlf.codigos

    def contains(self, texto):
        texto = texto.lower()
        if texto in self.name:
            return True
        elif any([c.contains(texto) for c in self.categorias]):
            return True
        elif self.hlf.contains(texto):
            return True
        return False

    def check_code(self, code):
        return self.hlf.check_code(code)

    def match_hlf(self, hlf):
        if hlf.descripcion:
            dosis = hlf.descripcion[-1]
            elem = dosis.descripcion.lower()
            for i, h in enumerate(self.hlf.descripcion):
                if elem == h.descripcion and dosis.cantidad == h.cantidad:
                    return i, True
        return 0, False
