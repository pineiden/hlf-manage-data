sed 's/volculo/volvulo/g' DATA_HLF_MDS_2.csv
sed 's/volculo/volvulo/g' DATA_HLF_MDS_2.csv |grep volculo
sed -i 's/izqueirdo/IZQUIERDO/gI' DATA_HLF_MDS_2.csv
sed -i "s/volculo sigmoides/VOLVULO SIGMOIDES/sI" DATA_HLF_MDS_2.csv
sed -i "s/volculo sigmoides/VOLVULO SIGMOIDES/gI" DATA_HLF_MDS_2.csv
sed -i "s/volculo sigmoides/VOLVULO DE SIGMOIDES/sI" DATA_HLF_MDS_2.csv
sed -i "s/volculo sigmoides/VOLVULO DE SIGMOIDES/gI" DATA_HLF_MDS_2.csv
sed -i "s/volculo sigmoide/VOLVULO DE SIGMOIDES/gI" DATA_HLF_MDS_2.csv
sed -i "s/volvulo de sigmoide/VOLVULO DE SIGMOIDES/gI" DATA_HLF_MDS_2.csv
sed -i "s/volvulo sigmoides/VOLVULO DE SIGMOIDES/gI" DATA_HLF_MDS_2.csv
sed -i "s/yumor/TUMOR/gI" DATA_HLF_MDS_2.csv
sed -i "s/izq\. /IZQUIERDA/gI" DATA_HLF_MDS_2.csv
sed -i "s/der\. /DERECHA/gI" DATA_HLF_MDS_2.csv
sed -i "s/der\.? /DERECHA/gI" DATA_HLF_MDS_2.csv
sed -i "s/izq\.? /IZQUIERDA/gI" DATA_HLF_MDS_2.csv
sed -i "s/sigmoidess/SIGMOIDES/gI" DATA_HLF_MDS_2.csv
sed -i "s/emb\.?/embarazo/gI" DATA_HLF_MDS_2.csv
sed -i "s/^tu /tumor/gI" DATA_HLF_MDS_2.csv
sed -i -r 's/(\d*)(\+)(\d*)/\1 \2 \3/g' DATA_HLF_MDS_2.csv
sed -i -E 's/([[:digit:]]+)([[:alpha:]])/\1 \2/g' DATA_HLF_MDS_2.csv

