#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pickle
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    path = Path(__file__).parent / "atenciones.data"
    databytes = path.read_bytes()
    total = pickle.loads(databytes)
    print("Total", total)
    hist,bins = np.histogram(total)
    print(hist, bins)

    n, bins, patches = plt.hist(total, bins=[0,10,20,30,40,50,60,70, 80, 90, 100],
             rwidth=0.9) 
    plt.title("Cantidad de medicaciones por paciente")
    plt.xlabel("Cantidad medicación")
    plt.ylabel("Cantidad pacientes")
    plt.savefig("cantidad_med.png")
    plt.show()


"""

Ideas analizar ventanas de recetas entre una receta y otra
Relaciones entre sala y medicamento
"""
