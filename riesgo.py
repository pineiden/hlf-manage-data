from enum import IntEnum


class Riesgo(IntEnum):
    nulo = 1
    bajo = 2
    medio = 3
    alto = 4

    @classmethod
    def create(cls, value):
        return cls(int(value))
