from one_hot_vector import OneHotVector
from riesgo import Riesgo
from prioridad import Prioridad
from categoria import Categoria
from dosis import Dosis
from pydantic.dataclasses import dataclass
import re
from typing import List, Dict, Callable, Tuple
from attrs import field, define


def onehot_encodig(data, field_callback: Callable):
    chars = len(data)
    tabla = {}
    for i, elem in enumerate(data):
        tabla[field_callback(elem)] = OneHotVector.create(data, i)
    return tuple([(k, v) for k, v in tabla.items()])


def analiza_med(name, texto):
    names = name.split("+")
    medicamentos = {k.strip(): [] for k in names}
    valores = []
    [valores.extend([m.strip() for m in v.strip().split("+")])
     for v in texto.split(",")]
    descripciones = []
    for k, values in medicamentos.items():
        for v in valores:
            info = v.replace(k, "").strip()
            dosis = Dosis.create(info)
            values.append(info)
            descripciones.append(dosis)
    return descripciones


@define(slots=True)
class HLF:
    descripcion: Tuple[Dosis, ...]
    codigos: Tuple[str, ...]
    tabla: Tuple[Tuple[str, OneHotVector], ...]

    @classmethod
    def create_tabla(cls, codigos):
        def field_callback(x): return x
        return onehot_encodig(codigos, field_callback)

    @classmethod
    def create(cls, name, descripcion, codigos):
        descripcion = descripcion.lower()
        parens = re.compile("\(.*\)$")
        name = name.lower().strip()
        name = re.sub(parens, "", name).strip()
        regex = re.compile("(\d+),(\d+)")
        last_colon = re.compile(",$")
        descripcion = re.sub(regex, r"\1.\2", descripcion)
        descripcion = [d.replace(f"{name} ", "").strip() for d in
                       descripcion.split(name)
                       if len(d.replace(f"{name} ", "").strip())
                       > 0]
        descripcion = [re.sub(last_colon, "", d) for d in descripcion]
        descripciones = []
        for d in descripcion:
            descripciones += analiza_med(name, d)
        descripciones = tuple(descripciones)
        codigos = tuple([c.strip() for c in codigos.split(",")])
        opts = {
            "descripcion": descripciones,
            "codigos": codigos,
            "tabla": cls.create_tabla(codigos)
        }
        return cls(**opts)

    def contains(self, texto):
        if any([d.contains(texto) for d in self.descripcion]):
            return True
        return False

    def check_code(self, code):
        return code.upper() in self.codigos

    @property
    def tabla_dict(self):
        return dict(self.tabla)

    def one_hot(self, code):
        return self.tabla_dict.get(code)

    @ property
    def mapa(self):
        return dict(zip(self.codigos, self.descripcion))
