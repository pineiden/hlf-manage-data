from enum import IntEnum


class Prioridad(IntEnum):
    baja = 0
    media = 1
    media_mas = 2
    alta = 3

    @classmethod
    def create(cls, value):

        __names = {
            "baja": 0,
            "media": 1,
            "media+": 2,
            "alta": 3
        }

        value = value.lower().strip()
        int_val = __names.get(value, 0)
        return cls(int_val)
