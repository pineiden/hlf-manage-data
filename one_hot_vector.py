from pydantic.dataclasses import dataclass
from attrs import field, define
import numpy as np
from functools import reduce


@define(slots=True, frozen=True)
class OneHotVector:
    names: list[str]
    value: np.ndarray

    @classmethod
    def create(cls, names: list[str], position: int):
        vector = np.zeros(len(names)).astype(int)
        if position < len(names):
            vector[position] = 1
            return cls(names, value=vector)
        return cls(names, value=vector)

    @property
    def chars(self):
        return self.value.size

    def todict(self):
        return dict(zip(self.names, self.value))

    def __repr__(self):
        return repr(dict(zip(self.names, self.value)))

    def __str__(self):
        return "".join(self.value.astype(str))

    def __eq__(self, other):
        return all(self.value == other.value)

    def __or__(self, other):
        cls = type(self)
        assert isinstance(
            other, OneHotVector), "Valor debe ser tipo OneHotVector"
        assert self.names == other.names, "Debe tener la misma serie de características"
        return cls(self.names, self.value | other.value)

    def __mul__(self, other):
        cls = type(self)
        assert isinstance(
            other, OneHotVector), "Valor debe ser tipo OneHotVector"
        assert self.names == other.names, "Debe tener la misma serie de características"
        val = self.value & other.value
        return cls(self.names, val)

    def __and__(self, other):
        cls = type(self)
        assert isinstance(other, OneHotVector), "Valor debe ser tipo OneHot"
        assert self.names == other.names, "Debe tener la misma serie de características"
        val = self.value & other.value
        return cls(self.names, val)

    def __len__(self):
        return sum([v for v in self.value if v == 1])

    @property
    def toint(self):
        ones = np.array([i for i, c in enumerate(self.value) if c == 1])
        exp = 2**ones
        total = reduce(lambda a, b: a | b, exp)
        return total
