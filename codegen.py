from attrs import define, field
from typing import List, Dict, Set, Optional, Any, Tuple
from one_hot_vector import OneHotVector
from leer_ppios_activos import (
    DatasetPrincipiosActivos)
from functools import reduce


Especialidad = Tuple[str, OneHotVector]
Receta = Tuple[str, OneHotVector]


@define(slots=True)
class CodeGen:
    mapa_especialidad: Dict[str, Especialidad]
    mapa_receta: Dict[str, Especialidad]
    principios: DatasetPrincipiosActivos
    dataset: List[Any]
    recetas: Dict[str, OneHotVector]
    especialidad: Dict[str, OneHotVector]

    @classmethod
    def create(cls,
               mapa_especialidad,
               mapa_receta,
               principios,
               dataset):

        def get_one_hot(code=""):
            result = principios.mapa.get(code)
            if result:
                return result.one_hot

        def get_code(mapa, key):
            codes = [ht for code in mapa.get(key)
                     if isinstance((ht := get_one_hot(code)),
                                   OneHotVector)]

            if codes:
                return reduce(lambda a, b: a | b, codes)

        def code_especialidad(especialidad):
            return get_code(mapa_especialidad, especialidad)

        def code_receta(receta):
            return get_code(mapa_receta, receta)

        especialidad = {}
        for esp in mapa_especialidad.keys():
            result = code_especialidad(esp)
            especialidad[esp] = result
        # especialidad = {especialidad: code_especialidad(especialidad)
        #                 for especialidad in mapa_especialidad.keys()}

        recetas = {}

        for rec in mapa_receta.keys():
            receta = code_receta(rec)
            recetas[rec] = receta
        # recetas = {receta: code_receta(receta)
        #            for receta in mapa_receta.keys()}

        # mapa_especialidad = tuple([(k, v)
        #                            for k, v in mapa_especialidad.items()])
        # mapa_receta = tuple([(k, v) for k, v in mapa_receta.items()])

        return cls(
            mapa_especialidad,
            mapa_receta,
            principios,
            dataset,
            recetas,
            especialidad)

    def code_especialidad(self, especialidad):
        """
        Codigo onehot operado con todos los medicamentos que estan
        asociados a espeespecialidad

        """

        codes = [ht for code in self.dict_especialidad.get(especialidad)
                 if isinstance((ht := self.get_one_hot(code)), OneHotVector)]

        if codes:
            return reduce(lambda a, b: a | b, codes)

    def code_receta(self, receta):
        """
        Codigo onehot operado con todos los medicamentos que estan
        asociados a espeespecialidad
        """
        codes = [ht for code in
                 self.dict_recetas.get(receta)
                 if isinstance((ht := self.get_one_hot(code)), OneHot)]

        if codes:
            return reduce(lambda a, b: a | b, codes)

    @property
    def dict_especialidad(self):
        return self.especialidad

    @property
    def dict_recetas(self):
        return self.recetas

    def get_one_hot(self, code=""):
        results = self.principios.search_code(code)
        ppio = [f for f in results.lista]
        if ppio:
            return ppio[0].one_hot

    def get_especialidad(self, code: str = "") -> Optional[OneHotVector]:
        return self.dict_especialidad.get(code)

    def get_receta(self, code: str = "") -> Optional[OneHotVector]:
        return self.dict_recetas.get(code)

    def get_data(self, code: str, field: str) -> Dict[str, Any]:
        for data in self.dataset:
            value = data.get(field)
            if code == value:
                return data
        return {}
