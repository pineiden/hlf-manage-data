from pydantic.dataclasses import dataclass


@dataclass(frozen=True)
class Categoria:
    descripcion: str

    def contains(self, texto):
        if texto in self.descripcion:
            return True
        return False
