from one_hot_vector import OneHotVector
from pathlib import Path
import pickle
from rich import print


def read_nlp_study():
    path = Path.cwd() / "diags_map.data"
    databytes = path.read_bytes()
    dataset = pickle.loads(databytes)
    return dataset


if __name__ == "__main__":
    dataset_nlp = read_nlp_study()
    bag = dataset_nlp["bag"]
    dataset = []
    for d, item in dataset_nlp["reference"].items():
        ohv = OneHotVector(names=bag, value=item["vector"])
        del item["lemmas"]
        del item["vector"]
        item["diagnostico_oh"] = ohv
        dataset.append(item)

    for d in dataset:
        print(d)
