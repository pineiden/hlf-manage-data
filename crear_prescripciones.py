#!/usr/bin/env python3
from __future__ import annotations
import re
import time
import json
import csv
import pickle
import zipfile

from zipfile import ZipFile
from datetime import datetime
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import typer
from datetime import datetime

from leer_ppios_activos import (
    DatasetPrincipiosActivos)

from principio_activo import (
    PrincipioActivo,
    FrozenPrincipioActivo)
from asignacion import Asignacion
from one_hot import OneHot
#from diagnostico import Diagnostico
from codegen import CodeGen
from prescripcion import Prescripcion
from rich import print

from enum import IntEnum
from typing import List, Dict, Set, Optional, Any, Tuple
from collections import defaultdict
from itertools import groupby
from pydantic.dataclasses import dataclass
from functools import reduce
from attrs import define, field
from functools import cached_property
from normalizar_texto import normalize
from hurry.filesize import size
from slugify import slugify
import json


def read_prescripciones(name):
    zpath = f"prescripciones/{name}.zip"
    with ZipFile(zpath, "r") as myzip:
        with myzip.open(f"{name}.data", "r") as myfile:
            return pickle.loads(myfile.read())


def save_prescripciones(prescripciones):
    path = Path.cwd() / "prescripciones"
    print("Saving prescripciones", len(prescripciones),
          type(prescripciones))
    # databytes = pickle.dumps(prescripciones)
    # save_file(path, databytes)
    prescripciones.sort(key=lambda item: slugify(item.principio.name))
    counter = 0
    dataset_guia = []
    compression = zipfile.ZIP_BZIP2
    compress_level = 6 if zipfile.ZIP_BZIP2 else 1
    for grupo, valores in groupby(prescripciones, key=lambda item: slugify(item.principio.name)):
        valores = list(valores)
        one = valores[-1]
        item = {"name": grupo,
                "total": len(valores)}
        dataset_guia.append(item)
        counter += 1
        print(grupo, len(valores))
        name = grupo
        zpath = path / f"{name}.zip"
        databytes = pickle.dumps(list(valores))
        start = datetime.utcnow()
        with ZipFile(zpath, 'w') as myzip:
            print(counter, zpath)
            myzip.writestr(
                zinfo_or_arcname=str(f"{name}.data"),
                data=databytes,
                compress_type=compression,
                compresslevel=compress_level)
        end = datetime.utcnow()
        delta = (end-start).total_seconds()
        print(f"ZIP {zpath} demoró {delta} secs")

    path_guia = path / "guia.txt"
    savetxt = json.dumps(dataset_guia)
    path_guia.write_text(savetxt)
    print("Saved guia prescripciones at ", path_guia)


def save_file(path, databytes):
    total_size = len(databytes) / 1024**2
    print(f"{path.name} :: Tamaño archivo {total_size} MB")
    zpath = path.parent / f"{path.name}.zip"
    with ZipFile(zpath, 'w') as myzip:
        myzip.writestr(
            zinfo_or_arcname=str(path.name),
            data=databytes)


def todatetime(texto, formato):
    """
    Pasa fecha en texto a datetime
    """
    try:
        return datetime.strptime(texto, formato)
    except Exception as exp:
        print(f"Texto de fecha es incorrecto <{texto}>")
        raise exp


def read_asignacion(base) -> Dict[int, Asignacion]:
    """
    Leer archivo de asignaciones
    """
    filename = base / "NEW_DATA_HLF.csv"
    dataset = {}
    with open(filename, 'r', encoding='utf8') as f:
        reader = csv.DictReader(f, delimiter=',')
        for row in reader:
            dataset[row["index"]] = Asignacion(row["frecuencia"],
                                               row["cantidad"],
                                               row["durante"])
    return dataset


def read_catalogo():
    """
    Leer el catalogo de principios activos
    """
    catalogo = Path.cwd() / "datasets/catalogo.data"
    databytes = catalogo.read_bytes()
    ppios = pickle.loads(databytes)
    return ppios


def main(filename: Path):
    """
    Función principal
    """

    asignaciones = read_asignacion(filename.parent)
    ppios = read_catalogo()
    # print(ppios)

    # assert 1==0,"Error"
    formato = '%d/%m/%Y %H:%M:%S'
    opts = {
        "PRES_FECHA": lambda x: todatetime(x.strip(), formato),
        "LINEA": lambda x: x.strip(),
    }

    dataset = []

    with filename.open('r') as f:
        reader = csv.DictReader(f, delimiter=',')
        rows = set()
        diagcode = set()
        salas = {}
        denominacion = set()
        administracion = set()
        count = 0
        pacientes = set()
        medicamentos = set()
        recetas = {}
        primer = None
        atenciones = {}
        especialidad = {}
        desplazar = {
            # "DIAGCODE": "CODIGO_MEDICAMENTO",
            "CODIGO_MEDICAMENTO": "ESTADO_PRESCR",
        }

        def cambio(row):
            data = dict(**row)
            for key1, key2 in desplazar.items():
                comodin = row[key2]
                data[key1] = comodin
            return data

        def data_kwargs(row):
            try:
                data = dict(fecha=row["PRES_FECHA"],
                            codigo_receta=row["CODIGO"],
                            codigo_hospitalizacion=row["EPISCODE"],
                            codigo_medicamento=row["CODIGO_MEDICAMENTO"],
                            especialidad=row["ESPECIALIDAD"],
                            index=row["index"],
                            # diagnostico=Diagnostico.create(
                            #     normalize(row["DIAGDESC"]))
                            )
                return data
            except Exception as exc:
                print(exc)
                raise exc
        diagnosticos = set()
        counter = 0
        start = datetime.utcnow()
        for line, row in enumerate(reader):
            # row = cambio(row)
            # if line == 20:
            #     break
            # pass
            for key, value in row.items():
                if key in opts:
                    try:
                        if not primer:
                            primer = dict(**row)
                        row[key] = opts[key](value)

                        rows.add(len(row))
                        if row["PATIENT_ID"] not in atenciones:
                            atenciones[row["PATIENT_ID"]] = []
                        atenciones[row["PATIENT_ID"]].append(row["DIAGDESC"])
                        diagcode.add(row["DIAGCODE"])
                        if (not row["CODIGO"] in recetas and row["CODIGO"] != ''):
                            recetas[row["CODIGO"]] = {
                                row["CODIGO_MEDICAMENTO"], }
                        else:
                            recetas[row["CODIGO"]].add(
                                row["CODIGO_MEDICAMENTO"])

                        if (not row["ESPECIALIDAD"] in especialidad
                                and row["ESPECIALIDAD"] != ''):
                            especialidad[row["ESPECIALIDAD"]] = {
                                row["CODIGO_MEDICAMENTO"], }
                        else:
                            especialidad[row["ESPECIALIDAD"]].add(
                                row["CODIGO_MEDICAMENTO"])

                        denominacion.add(row["PRES_DENOMINACION"])
                        administracion.add(
                            row["IND_ADMINISTRACION_1"].lower())
                        administracion.add(
                            row["IND_ADMINISTRACION_2"].lower())
                        pacientes.add(row["PATIENT_ID"])
                        count += 1
                        medicamentos.add(row["CODIGO_MEDICAMENTO"])
                    except Exception as exc:
                        print("Problema en fila", exc)
                        print(row)
                        continue
            counter += 1
            if counter % 100000 == 0:
                now = datetime.utcnow()
                print("Procesando", counter/100000)
                print("Elapsed", (now-start).total_seconds(), "[s]")
            data = data_kwargs(row)
            dataset.append(data)

        total = np.array([len(values) for values in
                          atenciones.values()])

        path = Path.cwd() / "datasets/atenciones.data"
        databytes = pickle.dumps(total)
        path.write_bytes(databytes)
        especialidades = especialidad

        def save_inputs_codegen():
            path = Path.cwd() / "datasets/to_codegen.data"
            #databytes = pickle.dumps(to_codegen)
            #save_file(path, databytes)
            # path.write_bytes(databytes)
            print("Input codegen saved", path)

        def save_codegen(codegen):
            databytes = pickle.dumps(codegen)
            # path.write_bytes(databytes)
            print("Saved codegen")
            save_file(path, databytes)

        def read():
            codegen = None
            path = Path.cwd() / "datasets/codegen.data"
            if not path.exists():
                print("Creando codegen")
                # codegen = CodeGen.create(especialidad, recetas, ppios, dataset)
                codegen = CodeGen.create(
                    especialidad,
                    recetas,
                    ppios,
                    dataset)
                print("Codegen generador OK")
                return codegen
            databytes = path.read_bytes()
            codegen = pickle.loads(databytes)
            return codegen

        start_time = datetime.utcnow()
        codegen = read()
        end_time = datetime.utcnow()
        print("Codegen ok", (end_time - start_time).total_seconds(), "segundos")
        print("Dataset", len(dataset))
        prescripciones = []
        counter = 0
        lost_codes = {
            "principio": {},
            "especialidad": {},
            "codigo_receta": {}
        }
        total = len(dataset)
        print("Iterando dataset")
        especialidades = codegen.dict_especialidad
        recetas = codegen.dict_recetas

        # crear prescripciones
        for i, data in enumerate(dataset):
            if i % 50000 == 0:
                print(f"Lectura {i}/{total}")
            try:
                especialidad = especialidades.get(data["especialidad"])
                receta = recetas.get(data["codigo_receta"])
                if especialidad and receta:
                    asignacion = asignaciones.get(data["index"])
                    if asignacion:
                        data["asignacion"] = asignacion
                        data["code_especialidad"] = especialidad
                        data["code_receta"] = receta
                        ppio = ppios.mapa.get(data["codigo_medicamento"])
                        if ppio:
                            data["principio"] = ppio.frozen()
                            prescripcion = Prescripcion(**data)
                        # print(prescripcion.to_dict())

                            if prescripcion:
                                prescripciones.append(prescripcion)
                                # diagnosticos.add(
                                #    prescripcion.diagnostico.texto)
                            else:
                                print(prescripcion,
                                      "No prescripcion para", data, counter)

                        else:

                            # print("Principio no existe,
                            # CODIGO:",data["codigo_medicamento"])
                            if data["codigo_medicamento"] not in lost_codes["principio"]:
                                lost_codes["principio"][data["codigo_medicamento"]] = 1
                            else:
                                lost_codes["principio"][data["codigo_medicamento"]] += 1

                            # print(len(lost_codes))
                            # print(data)
                else:
                    counter += 1
                    if especialidad is None:
                        field = "especialidad"
                        code = data[field]

                        if code not in lost_codes[field]:
                            lost_codes[field][code] = 1
                        else:
                            lost_codes[field][code] += 1

                    if receta is None:
                        """
                        Hay codigos de medicamentos que no esta en el registro
                        """
                        field = "codigo_receta"
                        code = data[field]

                        if code not in lost_codes[field]:
                            lost_codes[field][code] = 1
                        else:
                            lost_codes[field][code] += 1

            except Exception as e:
                print("Load pprescripcion l235")
                print(e)
                raise e

        print("Guardando prescripciones", len(prescripciones))
        path = Path.cwd() / "datasets/prescripciones.data"
        save_prescripciones(prescripciones)

        print("Guardando codigos medicamentos perdidos",
              len(lost_codes["principio"]))
        path = Path.cwd() / "datasets/lost_codes.json"
        datatxt = json.dumps(lost_codes)
        path.write_text(datatxt)

        print("Guardando diagnosticos",
              len(diagnosticos))
        path = Path.cwd() / "datasets/diagnosticos.data"
        databytes = pickle.dumps(diagnosticos)
        save_file(path, databytes)

        print("Saving codegen")
        save_codegen(codegen)


if __name__ == "__main__":
    typer.run(main)
