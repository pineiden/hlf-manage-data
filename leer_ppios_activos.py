from __future__ import annotations
from rich import print
import re
from typing import List, Dict, Callable, Tuple
from collections import defaultdict
from pydantic.dataclasses import dataclass
from dataclasses import field
from attrs import define
from one_hot_vector import OneHotVector
from principio_activo import PrincipioActivo
from dosis import Dosis


@define(slots=True)
class DatasetPrincipiosActivos:
    lista: List[PrincipioActivo]
    onehot_map: Dict[int, OneHotVector]
    mapa: Dict[str, PrincipioActivo]

    @classmethod
    def create(cls, lista, onehot_map):
        mapa = {}
        # lista de PrincipioActivo
        for elem in lista:
            mapa.update({c: elem for c in elem.codigos})
            try:
                elem.set_one_hot(onehot_map.get(elem.fid))
            except Exception as e:
                print("Error al asignar onehot", e)
                raise e
        return cls(lista, onehot_map, mapa)

    def search(self, texto):
        results = [p for p in self.lista if p.contains(texto.lower())]
        return DatasetPrincipiosActivos(results,
                                        onehot_map=self.onehot_map,
                                        mapa={})

    def search_code(self, code):
        results = []
        ppios = self.mapa.get(code)
        if ppios:
            results.append(ppios)
        return DatasetPrincipiosActivos(results,
                                        onehot_map=self.onehot_map,
                                        mapa=self.mapa)

    def get(self, code):
        return self.mapa.get(code)

    def __iter__(self):
        return iter(self.lista)
