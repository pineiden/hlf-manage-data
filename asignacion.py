from pydantic.dataclasses import dataclass


@dataclass(frozen=True)
class Asignacion:
    frecuencia: float
    cantidad: float
    durante: float

    def todict(self):
        return {
            "frecuencia": self.frecuencia,
            "cantidad": self.cantidad,
            "durante": self.durante}

    @property
    def fields(self):
        return (self.frecuencia, self.cantidad, self.durante)
