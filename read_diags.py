from pathlib import Path
import pickle
import typer


def read_diags():
    ppath = Path.cwd() / "diags_map.data"
    databytes = ppath.read_bytes()
    diags = pickle.loads(databytes)
    return diags


def run():
    diags = read_diags()
    print(diags.keys())
    for v in diags["vectores"]:
        print(v, sum(v))


if __name__ == "__main__":
    typer.run(run)
