from pathlib import Path
import pickle
from leer_ppios_activos import (
    DatasetPrincipiosActivos)

from principio_activo import (
    PrincipioActivo,
    FrozenPrincipioActivo)
from asignacion import Asignacion
from one_hot import OneHot
from diagnostico import Diagnostico
from codegen import CodeGen
from prescripcion import Prescripcion
from rich import print


def read_inputs_codegen():
    path = Path.cwd() / "datasets/to_codegen.data"
    databytes = path.read_bytes()
    to_codegen = pickle.dumps(databytes)
    print("Input codegen readed", path)
    return to_codegen


if __name__ == "__main__":
    to_codegen = read_inputs_codegen()
    especialidad = to_codegen["especialidad"]
    recetas = to_codegen["recetas"]
    ppios = to_codegen["ppios"]
    dataset = to_codegen["dataset"]
    path = Path.cwd() / "datasets/codegen.data"
    codegen = CodeGen.create(especialidad, recetas, ppios, dataset)
    databytes = pickle.dumps(codegen)
    path.write_bytes(databytes)
