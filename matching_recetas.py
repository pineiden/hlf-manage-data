from rich import print
import csv
import typer
from datetime import datetime
import numpy as np
import pickle
from pathlib import Path
from leer_ppios_activos import (PrincipioActivo, HLF,
                                DatasetPrincipiosActivos, analiza_med)
from one_hot import OneHot
import time


def todatetime(texto, formato):
    try:
        return datetime.strptime(texto, formato)
    except Exception as e:
        print(f"Texto de fecha es incorrecto <{texto}>")
        raise e


def main(filename: str):
    formato = '%d/%m/%Y %H:%M:%S'
    opts = {
        "PRES_FECHA": lambda x: todatetime(x.strip(), formato),
        "LINEA": lambda x: todatetime(x.strip(), formato),
    }

    catalogo_path = Path.cwd() / "catalogo.data"
    read_bytes = catalogo_path.read_bytes()
    catalogo = pickle.loads(read_bytes)

    with open(filename, 'r') as f:
        reader = csv.DictReader(f, delimiter=',')
        desplazar = {
            "DIAGDESC": "CODIGO_MEDICAMENTO",
            "CODIGO_MEDICAMENTO": "ESTADO_PRESCR",
            "ESTADO_PRESCR": "PRES_DENOMINACION",
            "CODIGO_MEDICAMENTO": "DIAGDESC"
        }

        def cambio(row):
            data = dict(**row)
            for key1, key2 in desplazar.items():
                comodin = row[key2]
                data[key1] = comodin
            return data
        dataset = {}
        for row in reader:
            row = cambio(row)
            code = row["DIAGDESC"]
            patient = row["PATIENT_ID"]
            denominacion = row["PRES_DENOMINACION"]
            result = catalogo.search_code(code)
            row["ppios_activo"] = result
            episcode = row["EPISCODE"]
            fecha = row["PRES_FECHA"]
            for ppio in result:
                name = ppio.name.lower()
                hlf = HLF.create(name, denominacion, "")
                #print(hlf, ppio.match_hlf(hlf))
            if patient not in dataset:
                dataset[patient] = {}
            else:
                if episcode not in dataset[patient]:
                    dataset[patient][episcode] = {}
                if fecha not in dataset[patient][episcode]:
                    dataset[patient][episcode][fecha] = []
                dataset[patient][episcode][fecha].append(row)
                if len(dataset[patient][episcode][fecha]) >= 2:
                    pass  # print(dataset[patient])
        print(dataset)
        print("Cantidad de recetas")
        counter = []
        for patient, episcodes in dataset.items():
            for epi, recetas in episcodes.items():
                counter.append(len(recetas))
        print(counter)


if __name__ == "__main__":

    typer.run(main)
