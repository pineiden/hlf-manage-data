import unicodedata


def normalize(text: str):
    text = text.lower().strip()
    norma = "NFKD"
    text = unicodedata.normalize(norma, text)
    return "".join([c for c in text if c.isascii()])
