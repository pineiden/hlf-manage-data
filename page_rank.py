from rich import print
import copy

grafo = [
    {"id": 0, "nodo": "a", "vecinos": [1, 3]},
    {"id": 1, "nodo": "z", "vecinos": [0, 7]},
    {"id": 2, "nodo": "b", "vecinos": [0, 5, 6]},
    {"id": 3, "nodo": "c", "vecinos": [1, 5]},
    {"id": 4, "nodo": "d", "vecinos": [6, 9]},
    {"id": 5, "nodo": "e", "vecinos": [0, 2, 6]},
    {"id": 6, "nodo": "f", "vecinos": [1, 3, 5]},
    {"id": 7, "nodo": "g", "vecinos": [0, 4]},
    {"id": 8, "nodo": "h", "vecinos": [0, 1]},
    {"id": 9, "nodo": "i", "vecinos": [2, 5, 6]},
]


def vecinos(grafo, a):
    copia = [g for g in grafo if g["id"] in a["vecinos"]]
    return copia


def page_rank(a, grafo, d=0.85):
    grafo = copy.deepcopy(grafo)
    vcs = vecinos(grafo, a)
    if a in grafo:
        grafo.remove(a)
    pr = (1-d) + d * sum([page_rank(v, grafo)/leng
                          for v in vcs if (leng := len(vecinos(grafo, v))+1) > 0])
    return pr


if __name__ == "__main__":
    a = grafo[0]
    print(vecinos(grafo, a))
    for a in grafo:
        pr = page_rank(a, grafo)
        print("Page Rank", a["nodo"], pr)
