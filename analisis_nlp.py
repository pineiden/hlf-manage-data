import csv
from datetime import datetime
import asyncio
from functools import reduce, partial
import stanza
# from nltk.stem.wordnet import WordNetLemmatizer
# from nltk.stem import SnowballStemmer
# from nltk.stem import PorterStemmer
from sentence_transformers import SentenceTransformer
import pickle
from pathlib import Path
from rich import print
import time
import json
# import nltk
# from nltk.tokenize import word_tokenize
# nltk.download(“punkt”)
# nlp = spacy.load("es_core_news_sm")
# nltk.download(“punkt”)
# ps = SnowballStemmer(“spanish”)
# lemmatizer = WordNetLemmatizer()
import concurrent.futures
import spacy_stanza
from normalizar_texto import normalize
from one_hot_vector import OneHotVector
import numpy as np
# from multiprocessing import set_start_method
# try:
#     set_start_method('spawn')
# except RunTimeError:
#     pass


def remove_punct(token):
    return [word for word in token if word.isalpha()]


# model = SentenceTransformer('paraphrase-multilingual-mpnet-base-v2')

def clean(word):
    letras = [chr(i) for i in range(ord('a'), ord('z')+1)]  # + \
    #[f"{i}" for i in range(10)]
    return "".join([w for w in word if w in letras])


# def op_words(doc):
#     words = [t.orth_ for t in doc if not t.is_punct | t.is_stop]
#     lexical = {t.lower() for t in words if len(t) > 3 and t.isalpha()}
#     if lexical:
#         return lexical
#     else:
#         words = {t.lower() for t in words if t.isalpha()}
#         return words


def op_words(doc):
    sentence_data = {
        "chars": len(doc.text),
        "len": len(doc.sentences),
        "words": sum([len(s.words) for s in doc.sentences]),
        "verbs": 0,
        "noun": 0,
        "adj": 0,
        "adv": 0,
        "others": 0,
        "numeric": 0,
        "prons": 0,
        "conj": 0,
        "propn": 0,
        "lemmas": set()
    }

    words = []
    omit = {"PUNCT", "STOP", "DET", "ADP"}
    for s in doc.sentences:
        for w in s.words:
            if w.upos not in omit:
                # contar verbs
                if w.upos == "VERB":
                    sentence_data["verbs"] += 1
                # contar nounes
                elif w.upos == "NOUN":
                    sentence_data["noun"] += 1
                elif w.upos == "PROPN":
                    sentence_data["propn"] += 1
                # contar adj
                elif w.upos == "ADJ":
                    sentence_data["adj"] += 1
                elif w.upos == "ADV":
                    sentence_data["adv"] += 1
                elif w.upos == "NUM":
                    sentence_data["numeric"] += 1
                elif w.upos == "PRON":
                    sentence_data["prons"] += 1
                elif w.upos in ("CCONJ", "SCONJ"):
                    sentence_data["conj"] += 1
                else:
                    sentence_data["others"] += 1

                # countar adv
                if w.lemma:
                    ww = clean(w.lemma)
                    if len(ww) > 1 and "&" not in ww:
                        sentence_data["lemmas"].add(ww)
                        words.append(len(w.text))

    if len(words) > 0:
        sentence_data["mean_len_words"] = np.mean(words)
        if len(words) > 1:
            sentence_data["std_len_words"] = np.std(words)
        else:
            sentence_data["std_len_words"] = 0
    else:
        sentence_data["mean_len_words"] = 0
    return sentence_data


def process_sentences(dataset, nlp, k):
    print(f"Processing grupo {k}")
    start = datetime.utcnow()
    newdataset = []
    try:
        for sentence in dataset:
            analisis = nlp(sentence)
            mineria = op_words(analisis)
            newdataset.append((sentence, mineria))
    except Exception as e:
        print("Error al procesar pipeline nlp")
        raise e

    end = datetime.utcnow()
    secs = (end-start).total_seconds()
    print(f"Grupo {k} listo, durando {secs}")
    return newdataset


def bag_words(dataset):
    words = set()
    for k, lista in dataset.items():
        words |= lista["lemmas"]
        #set(reduce(lambda a, b: a | b, set_words))
    words = list(words)
    words.sort()
    return words


def convert(item):
    if isinstance(item, OneHotVector):
        return item.value
    else:
        return str(item)


def distribuir(lista, n=63):
    position = 0
    dataset = {i: set() for i in range(n)}
    for w in lista:
        dataset[position].add(w)
        position += 1
        if position >= n:
            position = 0
    return dataset


async def amain(dataset, nlp):

    loop = asyncio.get_running_loop()
    dataset_distribuido = distribuir(dataset, n=32)

    results = []

    with concurrent.futures.ThreadPoolExecutor(max_workers=32) as executor:
        for k, v in dataset_distribuido.items():
            print(f"Levantando ejecución {k}")
            result = loop.run_in_executor(
                executor, partial(process_sentences, v, nlp, k))
            results.append(result)

        final_results = [await r for r in results]

        print("Entegando Resultados finales...")

        while not (ok := all([r.done() for r in results])):
            print(ok)
            print("All done?", ok, datetime.utcnow())
            await asyncio.sleep(6)

        return final_results


if __name__ == '__main__':
    stanza.download("es")
    # para evitar llamads circulares no usar prescripciones sino
    # directamente el archivo fuente o de descripciones
    ppath = Path.cwd() / "DATA_HLF_MDS_2.csv"
    dataset = {}
    with ppath.open('r') as f:
        reader = csv.DictReader(f, delimiter=',')
        dataset = {normalize(r["DIAGDESC"]) for r in reader}

    #nlp = spacy_stanza.load_pipeline("es")
    nlp = stanza.Pipeline("es")

    # elementos únicos de texto
    # Sentences are encoded by calling model.encode()
    dataset = [d for d in dataset if d != '']
    print("Generando modelo")
    print("Cantidad diagnosticos diferentes", len(dataset))
    results = asyncio.run(amain(dataset, nlp))
    print("Results ok", len(results))

    dataset_nlp = {}
    for r in results:
        dataset_nlp.update(r)

    # dataset_nlp = {s: op_words(s) for s in dataset}
    bag = bag_words(dataset_nlp)

    # solo vectores con las posiciones en que da 1

    n = len(bag)
    # ira bien tirarle onhehot?=
    vectores = []
    vect_dict = {}
    rbag = bag[::-1]

    for k, mineria in dataset_nlp.items():
        lista = mineria["lemmas"]
        vector = np.array(
            [1 if b in lista else 0 for i, b in enumerate(rbag)])
        vectores.append(vector)
        mineria["vector"] = vector

    # vectores = {k: np.array([i for i, b in enumerate(bag) if b in lista])
    #             for k, lista in dataset_nlp.items()}

    diagnosticos = {
        "bag": rbag,
        "reference": dataset_nlp,
        "vectores": vectores
    }

    print("Total palabras diferentes", len(bag))
    print(bag)
    # embeddings = model.encode(dataset)

    # # Print the embeddings
    # embeds = []
    # # for sentence, embedding in zip(dataset, embeddings):
    # #     print("Sentence:", sentence)
    # #     print("Embedding:", embedding)
    # #     print("-")
    # #     embeds.append((sentence, embedding))
    path = ppath.parent / "diags_map.data"
    databytes = pickle.dumps(diagnosticos)
    path.write_bytes(databytes)
